<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\OpinionType;
use Symfony\Component\HttpFoundation\Request;

class OpinionController extends Controller
{
    /**
     * @Route("/opinion", name="opinion")
     */
    public function index(Request $req)
    {

        $form = $this->createForm(OpinionType::class);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()){
            $opinion = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($opinion);
            $em->flush();            
        }
        return $this->render('opinion.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
