<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\CategoryRepository;
use App\Entity\Category;

class EditCategoryController extends Controller
{
  //Ce controller permet de récuperer toutes les catégories dans la barre de nav
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Category::class);

        $category = $repo->findAll();
                dump($category);

        return $this->render(
            '_header.html.twig',
            array('category' => $category)
        );
    }
}
