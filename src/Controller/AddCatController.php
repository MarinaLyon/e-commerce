<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use App\Form\CategoryType;

class AddCatController extends Controller
{
    /**
     * @Route("/add/cat", name="add_category")
     */
    public function category(Category $category = null, Request $request, FileUploader $fileUploader)
    {
        if (!$category) {
            $category = new Category();
        }

        $editMode = $category->getId() !== null;

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render('add_category.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'editMode' => $editMode
        ]);
    }


    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(Category $category)
    {

        $em = $this->getDoctrine()->getManager(); //em = entity manager, interne à doctrine
        $em->remove($category);
        $em->flush();
        return $this->redirectToRoute("home");
    }

}
