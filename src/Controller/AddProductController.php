<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AddProductController extends Controller
{
  /**
   * @Route("/add/product", name="add_product")
   * @Route("/add/product/{id}/edit", name="edit")
   */

  public function product(Product $product = null, Request $request, FileUploader $fileUploader)
  {
    if (!$product) {
      $product = new Product();
    }

    $editMode = $product->getId() !== null;

    if ($editMode) {
      $path = $fileUploader->getTargetDirectory();
      $fileName = $product->getImage();
      $file = new UploadedFile($path . $fileName, $fileName);
      $product->setImageFile($file);
    }

    $form = $this->createForm(ProductType::class, $product);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $file = $product->getImageFile();

      $fileName = $fileUploader->upload($file);

      $product->setImage($fileName);
      // $repo->add($form->getData());
      $em = $this->getDoctrine()->getManager();

      $em->persist($product);
      $em->flush();

      return $this->redirectToRoute("home");
    }

    return $this->render('add_product.html.twig', [
      'product' => $product,
      'form' => $form->createView(),
      'editMode' => $editMode
    ]);
  }


  /**
   * @Route("/delete/{id}", name="delete")
   */
  public function delete(Product $product)
  {

    $em = $this->getDoctrine()->getManager(); //em = entity manager, interne à doctrine
    $em->remove($product);
    $em->flush();
    return $this->redirectToRoute("home");
  }

  
}
