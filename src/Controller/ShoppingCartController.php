<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductType;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ShoppingCart;
use App\Entity\Category;
use App\Entity\LineShoppingCart;


class ShoppingCartController extends Controller
{
    /**
     * @Route("user/shopping_cart", name="shopping_cart")
     */
    public function index(ObjectManager $manager)
    {

        $user = $this->getUser();

        if (!$user->getShoppingCart()) {//si le panier n'existe pas encore car vide
            $shoppingCart = new ShoppingCart();//crée une nouvelle instance de ShoppingCart
            $user->setShoppingCart($shoppingCart);
        }
        $shoppingCart = $user->getShoppingCart();

        $shoppingCart->setTotalPrice(0);
        foreach ($shoppingCart->getLineShoppingCarts() as $lineShoppingCart) {
            $shoppingCart->setTotalPrice($shoppingCart->getTotalPrice() + ($lineShoppingCart->getPrice() * $lineShoppingCart->getQuantity()));
        }

        $manager->persist($shoppingCart);// je dis à Doctrine que je veux (éventuellement) sauvegarder ce panier (pas encore de requête)user/shopping_cart
        $manager->flush();// Je demande à Doctrine d'éxecuter réellement la requête

        dump($shoppingCart->getLineShoppingCarts());

        return $this->render("shopping_cart.html.twig", [

            "shoppingCart" => $shoppingCart,
        ]);
    }
    
    /**
     * @Route("/deleteLSC/{id}", name="deleteLSC")
     */
    public function deleteLineShoppingCart(LineShoppingCart $lineShoppingCart)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($lineShoppingCart);
        $em->flush();
        return $this->redirectToRoute("shopping_cart");
    }
}
