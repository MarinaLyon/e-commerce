<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalPrice;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineShoppingCart", mappedBy="ShoppingCart", cascade={"persist"})
     */
    private $lineShoppingCarts;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="shoppingCart", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    

    public function __construct()
    {
        $this->lineShoppingCarts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTotalPrice(): ?int
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(?int $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }


    public function getTotalProducts(): ?int
    {
        return $this->totalProducts;
    }

    public function setTotalProducts(?int $totalProducts): self
    {
        $this->totalProducts = $totalProducts;

        return $this;
    }

    /**
     * @return Collection|LineShoppingCart[]
     */
    public function getLineShoppingCarts(): Collection
    {
        return $this->lineShoppingCarts;
    }

    public function addLineShoppingCart(LineShoppingCart $lineShoppingCart): self
    {
        if (!$this->lineShoppingCarts->contains($lineShoppingCart)) {
            $this->lineShoppingCarts[] = $lineShoppingCart;
            $lineShoppingCart->setShoppingCart($this);
        }

        return $this;
    }

    public function removeLineShoppingCart(LineShoppingCart $lineShoppingCart): self
    {
        if ($this->lineShoppingCarts->contains($lineShoppingCart)) {
            $this->lineShoppingCarts->removeElement($lineShoppingCart);
            // set the owning side to null (unless already changed)
            if ($lineShoppingCart->getShoppingCart() === $this) {
                $lineShoppingCart->setShoppingCart(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    
}
