<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Table(name="`user`")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 */


class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Username;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Opinion", mappedBy="author", orphanRemoval=true)
     */
    private $opinions;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="boolean")
     */
    private $admin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ShoppingCart", mappedBy="user", cascade={"persist", "remove"})
     */
    private $shoppingCart;

   

    public function __construct()
    {
        $this->opinions = new ArrayCollection();
        $this->admin = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail() : ? string
    {
        return $this->email;
    }

    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles()
    {
        if ($this->getAdmin()) {
            return ["ROLE_ADMIN"];
        }
        return ["ROLE_USER"];
    }

    public function getSalt()
    {

    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {

    }

    public function getPassword() : ? string
    {
        return $this->password;
    }

    public function setPassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }

    public function getName() : ? string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function setUsername(string $Username) : self
    {
        $this->Username = $Username;

        return $this;
    }

    /**
     * @return Collection|Opinion[]
     */
    public function getOpinions() : Collection
    {
        return $this->opinions;
    }

    public function addOpinion(Opinion $opinion) : self
    {
        if (!$this->opinions->contains($opinion)) {
            $this->opinions[] = $opinion;
            $opinion->setAuthor($this);
        }

        return $this;
    }

    public function removeOpinion(Opinion $opinion) : self
    {
        if ($this->opinions->contains($opinion)) {
            $this->opinions->removeElement($opinion);
            // set the owning side to null (unless already changed)
            if ($opinion->getAuthor() === $this) {
                $opinion->setAuthor(null);
            }
        }

        return $this;
    }

    public function getStreet() : ? string
    {
        return $this->Street;
    }

    public function setStreet(string $Street) : self
    {
        $this->Street = $Street;

        return $this;
    }

    public function getZipCode() : ? string
    {
        return $this->zip_code;
    }

    public function setZipCode(string $zip_code) : self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getCity() : ? string
    {
        return $this->city;
    }

    public function setCity(string $city) : self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry() : ? string
    {
        return $this->country;
    }

    public function setCountry(string $country) : self
    {
        $this->country = $country;

        return $this;
    }

    public function getAdmin() : ? bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin) : self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getShoppingCart(): ?ShoppingCart
    {
        return $this->shoppingCart;
    }

    public function setShoppingCart(?ShoppingCart $shoppingCart): self
    {
        $this->shoppingCart = $shoppingCart;

        // set (or unset) the owning side of the relation if necessary
        $newUser = $shoppingCart === null ? null : $this;
        if ($newUser !== $shoppingCart->getUser()) {
            $shoppingCart->setUser($newUser);
        }

        return $this;
    }

    
}
